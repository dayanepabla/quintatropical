$(function() {
  $container = $('.products-list');

  $container.isotope({
    itemSelector: '.products-list__item',
    layoutMode: 'fitRows',
    getSortData: {
      category: '[data-category]'
    }
  });

  $('.products-nav a').click(function(event) {
    event.preventDefault();

    var filterValue = $(this).attr('data-filter');
    $container.isotope({ filter: filterValue });
  });

  $('.products-list__item a').magnificPopup({ type: 'image' });
});
