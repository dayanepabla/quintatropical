$(document).ready(function() {

  $(".gallery-slider").owlCarousel({
      autoPlay: 3000,
      items : 4,
      itemsScaleUp: false,
      navigation: true,
      pagination: false,
  });

});
