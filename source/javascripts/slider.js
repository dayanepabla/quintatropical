$(function() {
  /*==============================
        Banner
  ==============================*/

  // full height screen
  function setHeightBanner() {
    var heightWindow = $(window).height();
    var widthWindow = $(window).width();

    $('#banner, .banner-slider').height(heightWindow);
  };

  setHeightBanner();
  $(window).bind('resize', function () {
    setHeightBanner();
  });

  $('.banner-slider').owlCarousel({
    items: 1,
    singleItem: true,
    itemsScaleUp:true,
    autoPlay: true,
    slideSpeed: 500,
    navigation: true,
    pagination: false,
    transitionStyle: 'fadeUp'
  });

  $('.about-slider').owlCarousel({
    singleItem: true,
    autoPlay: false,
    navigation: true,
    pagination: false,
    itemsScaleUp: true,
  });
});
