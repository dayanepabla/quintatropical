//= require jquery
//= require fotorama
//= require bootstrap
//= require OwlCarousel/owl-carousel/owl.carousel
//= require isotope/dist/isotope.pkgd
//=require magnific-popup/dist/jquery.magnific-popup

//= require slider
//= require products
//= require navbar
//= require gallery
