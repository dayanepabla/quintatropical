$(function() {
  $('.navbar-toggle').click(function(event) {
    event.preventDefault();

    if ($('.head-nav').css('right') !== '0px') {
      $('.head-nav').css('right', '0px');
      $(this).css('right', '165px');
    } else {
      $('.head-nav').css('right', '-170px');
      $(this).css('right', '0px');
    }
  })
});
